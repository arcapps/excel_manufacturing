from frappe import _

def get_data():
	return [
		{
			"module_name": "Excel Manufacturing",
			"type": "module",
			"label": _("Excel Manufacturing")
		}
	]
