// Copyright (c) 2023, Shaid Azmin and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["EISL Stock Availability Report"] = {
	filters: [
                {
                        fieldname:"item_name",
                        label: __("Item Name"),
                        fieldtype: "MultiSelectList",
						get_data: function (txt) {
						return frappe.db.get_link_options("Item", txt);
						},
                },
				{
					fieldname:"item_group",
					label: __("Item Group"),
					fieldtype: "MultiSelectList",
					get_data: function (txt) {
					return frappe.db.get_link_options("Item Group", txt);
					},
				},
				{
					fieldname:"brand",
					label: __("Brand"),
					fieldtype: "MultiSelectList",
					get_data: function (txt) {
					return frappe.db.get_link_options("Brand", txt);
					},
				},
   		             {
                        fieldname:"warehouse",
                        label: __("Warehouse"),
						fieldtype: "MultiSelectList",
						get_data: function (txt) {
						return frappe.db.get_link_options("Warehouse", txt);
						},
                },
				{
					fieldname: "show_zero_stock",
					label: __("Show Zero Stock"),
					fieldtype: "Check"
				},
				
	],   
	
};


