# Copyright (c) 2023, Shaid Azmin and contributors
# For license information, please see license.txt

import frappe

def execute(filters=None):
    columns, data = [], []
    columns = get_columns() 
    data = get_data(filters)
    return columns, data

def get_columns():
    columns = [
        {
            "label": ("Item Code"),
            "fieldname": "item_code",
            "fieldtype": "Link",
            "options":"Item",
            "width": 100
        },
        {
            "label": ("Item name"),
            "fieldname": "item_name",
            "fieldtype": "Data",
            "width": 300
        },
      
        {
            "label": ("Item Group"),
            "fieldname": "item_group",
            "fieldtype": "Data",
            "width": 150
        },  {
            "label": ("Brand"),
            "fieldname": "brand",
            "fieldtype": "Data",
            "width": 150
        },
        {
            "label": ("Warehouse"),
            "fieldname": "warehouse",
            "fieldtype": "Data",
            "width": 150
        },
        {
            "label": ("Current Quantity"),
            "fieldname": "actual_qty",
            "fieldtype": "Data",
            "width": 200
        },
    ]
    return columns

def get_data(filters):
    conditions = ""
    if filters.item_name:
        item_codes = "','".join(filters.item_name)
        conditions += f" AND tb.item_code IN ('{item_codes}')"
    if filters.item_group:
        item_groups = "','".join(filters.item_group)
        conditions += f" AND ti.item_group IN ('{item_groups}')"  
    if filters.brand:
        brands = "','".join(filters.brand)
        conditions += f" AND ti.brand IN ('{brands}')"    
    if filters.warehouse:
        warehouses = "','".join(filters.warehouse)
        conditions += f" AND tb.warehouse IN ('{warehouses}')"
    if filters.show_zero_stock==1:
        conditions += f" AND tb.actual_qty=0"
    else : 
        conditions += f" AND tb.actual_qty>0"   

    query = f"""
        SELECT tb.item_code, ti.item_name, ti.item_group, ti.brand, tb.warehouse, tb.actual_qty  
        FROM `tabBin` as tb
        LEFT JOIN `tabItem` as ti ON ti.item_code = tb.item_code
        WHERE 1=1 {conditions}
    """
    return frappe.db.sql(query, as_dict=True)

